# Link Search

## Prerequisites

* You need to have `curl`, `chromium`, and `recode` installed first.

## Usage

```shell
bash <(curl -sL https://tinyurl.com/get-online-info) "interesting facts"
```
