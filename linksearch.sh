#!/bin/bash

linksearch() {
	LINKSEARCH_TEXT_INPUT=${1:-$LINKSEARCH_TEXT_INPUT}

	if [ -z "$LINKSEARCH_TEXT_INPUT" ]; then
		echo "error: missing input text"
		return 1
	fi

	LINKSEARCH_TEXT_INPUT=$(printf "%b" "$LINKSEARCH_TEXT_INPUT" | tr ' ' '+')

	printf "%b" "`chromium --headless --disable-gpu --dump-dom "https://google.com/search?q=${LINKSEARCH_TEXT_INPUT}" 2>/dev/null | \
		sed -E 's#(\\x.{2})# \1 #g'`" 2>/dev/null | grep -Eoi '<a [^>]+>' | grep -Eo 'href="*[^]+[\"]*"*' | \
		grep -Eo '(http|https)://[^]+.*[/]*.*[^"]' | tr -d ' ' | uniq | recode html..utf8
}

linksearch "$@"
